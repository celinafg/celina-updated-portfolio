exports.createPages = async ({ actions, graphql, reporter }) => {
  const result = await graphql(`
    query {
      projects: allFile(
        filter: {
          extension: { regex: "/(mdx)/" }
          sourceInstanceName: { eq: "projects" }
        }
      ) {
        nodes {
          childMdx {
            frontmatter {
              slug
            }
          }
        }
      }
      blog: allFile(
        filter: {
          extension: { regex: "/(mdx)/" }
          sourceInstanceName: { eq: "blog" }
        }
      ) {
        nodes {
          childMdx {
            frontmatter {
              slug
            }
          }
        }
      }
      design: allFile(
        filter: {
          extension: { regex: "/(mdx)/" }
          sourceInstanceName: { eq: "design" }
        }
      ) {
        nodes {
          childMdx {
            frontmatter {
              slug
            }
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panic("failed to create posts", result.errors)
  }

  const postNodes = result.data.projects.nodes
  const blogNodes = result.data.blog.nodes
  const designNodes = result.data.design.nodes

  postNodes.forEach(post => {
    const slug = post.childMdx.frontmatter.slug
    actions.createPage({
      path: `/projects/${slug}`,
      component: require.resolve("./src/templates/projectpost.js"),
      context: {
        slug: slug,
      },
    })
  })

  blogNodes.forEach(post => {
    const slug = post.childMdx.frontmatter.slug
    actions.createPage({
      path: `/blog/${slug}`,
      component: require.resolve("./src/templates/blogpost.js"),
      context: {
        slug: slug,
      },
    })
  })

  designNodes.forEach(post => {
    const slug = post.childMdx.frontmatter.slug
    actions.createPage({
      path: `/design/${slug}`,
      component: require.resolve("./src/templates/designpost.js"),
      context: {
        slug: slug,
      },
    })
  })
}
