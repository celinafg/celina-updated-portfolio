import React from "react"
import styled from "@emotion/styled"
import {
  AiOutlineLinkedin,
  AiOutlineGitlab,
  AiOutlineMail,
} from "react-icons/ai"
import color from "../components/colors"
import { Link } from "gatsby"

const Contact = () => (
  <>
    <Wrapper>
      <ContactSection>
        <h2>stay in touch</h2>
        <p>thanks for visiting!</p>
        <Content>
          <p>
            both Foodie and Pawfect Pets have an admin tool. if you're keen to
            check them out, you can email me or message me on any of these
            accounts!
          </p>
          <IconSec>
            <A
              href="https://www.linkedin.com/in/celina-garcia-798086144/"
              target="_blank"
              rel="nofollow"
              className="linky"
            >
              <AiOutlineLinkedin />
            </A>
            <A
              href="https://gitlab.com/celinafg"
              target="_blank"
              rel="nofollow"
              className="linky"
            >
              <AiOutlineGitlab />
            </A>
            <A
              href="mailto:celina.btfg@gmail.com"
              target="_blank"
              rel="nofollow"
              className="linky"
            >
              <AiOutlineMail />
            </A>
          </IconSec>
          <NavLink to="/resume" className="link">
            view my resume
          </NavLink>
          <p>or</p>
          <Download className="link">
            <a href="cfgarcia-resume-20.pdf" download>
              <Download>download my resume</Download>
            </a>
          </Download>
        </Content>
      </ContactSection>
    </Wrapper>
  </>
)

const Download = styled.a`
  color: ${color.paper};
  font-size: 1rem;

  &:hover {
    color: ${color.mustard};
  }
`

const NavLink = styled(Link)`
  color: ${color.paper};
  font-size: 1.1rem;

  &:hover {
    color: ${color.mustard};
  }
`

const IconSec = styled.div`
  font-size: 50px;
  color: ${color.mint};
  padding: 4vh;
  justify-content: center;
`
const A = styled.a`
  margin-right: 15px;
  width: 48px;
  height: 48px;
  display: inline-block;
  background-position: 0 0;
  background-repeat: no-repeat;
  z-index: 2000;
  overflow: hidden;
  -webkit-box-shadow: 0 6px 19px 0 rgba(0, 0, 0, 0.24),
    0 4px 15px 0 rgba(0, 0, 0, 0.15);
  -moz-box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.24),
    0 4px 15px 0 rgba(0, 0, 0, 0.15);
  box-shadow: 0px 5px 11px 0px rgba(0, 0, 0, 0.24),
    0px 4px 15px 0px rgba(0, 0, 0, 0.15);

  &:hover {
    transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    transition: transform 0.86s ease-out;
    -webkit-transition: -webkit-transform 0.86s ease-out;
    -moz-transition: -moz-transform 0.86s ease-out;
  }
`
const Content = styled.div`
  margin-top: 4vh;
`
const ContactSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-basis: 50vw;
  text-align: center;
  @media (max-width: 768px) {
    flex-basis: 80vw;
  }
`

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  height: 70vh;
  width: 80vw;
  margin-bottom: 8vh;
  @media (max-width: 768px) {
    height: 100%;
    width: 100%;
    margin-top: 6vh;
    margin-bottom: 10vh;
  }
`

export default Contact
