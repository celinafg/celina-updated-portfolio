import React from "react"
import styled from "@emotion/styled"
import useDesignPosts from "../hooks/use-designposts"
import DesignPreview from "../components/design-preview"
import Layout from "../components/layout/layout"

const Design = () => {
  const posts = useDesignPosts()
  return (
    <>
      <Layout>
        <Wrapper>
          <Container>
            <Subhead>work and play</Subhead>
            <h2>design + art</h2>
            <Line className="extra" />
          </Container>
          <CardWrap>
            {posts.map(post => (
              <DesignPreview key={post.slug} post={post} />
            ))}
          </CardWrap>
        </Wrapper>
      </Layout>
    </>
  )
}
const Line = styled.hr`
  width: 7rem;
  height: 0.5vh;

  border: none;
`
const Subhead = styled.p`
  text-transform: uppercase;
  padding-bottom: 3vh;
  letter-spacing: 0.4rem;
  font-size: 0.9rem;
  width: 80vw;
  margin-left: 0;
  padding-top: 10vh;
`

const Wrapper = styled.section`
  height: 100%;
  width: 100%;

  @media (max-width: 768px) {
    margin: 12px;
    margin-top: 13px;
  }
`

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 10px;
  height: 100%;
  @media (max-width: 768px) {
    margin: 12px;
    margin-top: 13px;
  }
`
const CardWrap = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`
export default Design
