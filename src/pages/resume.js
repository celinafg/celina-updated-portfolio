import React from "react"
import Pdf from "@mikecousins/react-pdf"
import styled from "@emotion/styled"
import Layout from "../components/layout/layout"

import pdf from "../components/cfgarcia-resume-20.pdf"
const Resume = () => {
  return (
    <>
      <Layout>
        <Container>
          <a href={pdf} download>
            <Wrapper>
              <Heading>
                <a href={pdf} download>
                  Click anywhere on the document to download.
                </a>
              </Heading>

              <Pdf file={pdf} page={1} />
              <Pdf file={pdf} page={2} />
            </Wrapper>
          </a>
        </Container>
      </Layout>
    </>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  @media (max-width: 768px) {
    width: 100%;
  }
`
const Heading = styled.p`
  margin-left: 8vh;
  margin-top: 10vh;
  margin-bottom: 3vh;
  align-self: flex-start;
  @media (max-width: 768px) {
    margin-top: 40px;
    align-self: flex-start;
    margin-left: 12px;
  }
  //
`
const Container = styled.div`
  canvas {
    width: 90% !important;
    height: auto !important;
  }

  @media (max-width: 768px) {
    canvas {
      width: 93% !important;
      height: auto !important;
    }
  }
`
export default Resume
