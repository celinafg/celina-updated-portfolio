import React from "react"
import styled from "@emotion/styled"
import useBlogPosts from "../hooks/use-blogposts"
import BlogPreview from "../components/blog-preview"
import Layout from "../components/layout/layout"

const Blog = () => {
  const posts = useBlogPosts()
  return (
    <>
      <Layout>
        <Wrapper>
          <Subhead></Subhead>
          <Container>
            <h2>welcome to the blog</h2>
            <Line className="extra" />
            <CardWrap>
              {posts.map(post => (
                <BlogPreview key={post.slug} post={post} />
              ))}
            </CardWrap>
          </Container>
        </Wrapper>
      </Layout>
    </>
  )
}
const Line = styled.hr`
  width: 7rem;
  height: 0.5vh;
  border: none;
  margin-top: 6vh;
  margin-bottom: 4vh;
`
const Subhead = styled.p`
  text-transform: uppercase;
  padding-bottom: 3vh;
  letter-spacing: 0.4rem;
  font-size: 0.9rem;
  width: 80vw;
  padding-top: 10vh;
`

const Wrapper = styled.section`
  min-height: 100vh;

  width: 90%;
  margin-right: 10px;

  @media (max-width: 768px) {
    margin: 0 24px;
  }
`

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 10px;
`
const CardWrap = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: space-between;
`
export default Blog
