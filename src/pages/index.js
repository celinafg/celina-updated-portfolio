import React from "react"
import Layout from "../components/layout/layout"
import About from "./about"
import Projects from "./projects"
// import Blog from "./blog"
import Contact from "./contact"

// import Resume from "./resume"
// import Tech from "../../content/projects/tech"

export default () => {
  return (
    <>
      <Layout>
        <About />
        <Projects />
        {/* <Blog /> */}
        <Contact />
      </Layout>
    </>
  )
}
