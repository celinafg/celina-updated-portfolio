import React from "react"
import styled from "@emotion/styled"

const About = () => (
  <>
    <Wrapper>
      <AboutSection>
        <h2>about</h2>
        <Content>
          <P>hi there! I’m Celina, a frontend web developer and creative! </P>

          <P>
            ex-marketer, graphic design and branding enthusiast, and career
            shifter, I have an insatiable curiosity and a passion for learning
            new things. growing up, I was always the tinkerer and fix-it person
            at home, and that curiosity for seeing what’s under the hood, taking
            it apart, and putting it back together is the same feeling I get
            when making websites. building things excites me, whether it be
            brands or websites.
          </P>

          <P>
            when I’m not coding, I like to spend my time with my border collie
            best friend, boulder, be at the beach, work on passion projects or
            test recipes in the kitchen.
          </P>
        </Content>
      </AboutSection>
    </Wrapper>
  </>
)

const Content = styled.div`
  margin-top: 1vh;
  @media (max-width: 768px) {
    margin-top: 0;
  }
`
const AboutSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-basis: 50vw;
  text-align: center;
  @media (max-width: 768px) {
    flex-basis: 80vw;
    margin-top: 30px;
    align-items: flex-start;
    text-align: left;
  }
`

const Wrapper = styled.div`
  display: flex;
  height: 80vh;
  justify-content: center;
  width: 80vw;
  @media (max-width: 768px) {
    width: 96vw;
    height: 80%;
  }
`
const P = styled.p`
  margin-top: 20px;
`

export default About
