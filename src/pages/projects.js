import React from "react"
import styled from "@emotion/styled"
import useProjectPosts from "../hooks/use-projectposts"
import ProjectPreview from "../components/project-preview"

const Projects = () => {
  const posts = useProjectPosts()
  return (
    <>
      <Wrapper>
        <Container>
          <Subhead>From the portfolio</Subhead>
          <h2>selected projects</h2>
          <Line className="extra" />
          <CardWrap>
            {posts.map(post => (
              <ProjectPreview key={post.slug} post={post} />
            ))}
          </CardWrap>
        </Container>
      </Wrapper>
    </>
  )
}

const Line = styled.hr`
  margin-bottom: 4vh;
`
const Subhead = styled.p`
  text-transform: uppercase;
  padding-bottom: 3.5vh;
  letter-spacing: 0.4rem;
  font-size: 0.9rem;
  padding-top: 11vh;
`

const Wrapper = styled.section`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
  margin-top: 8vh;
  @media (max-width: 768px) {
    margin: 0 24px;
  }
`

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 10px;
`
const CardWrap = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`
export default Projects
