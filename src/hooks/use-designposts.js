import { graphql, useStaticQuery } from "gatsby"

const useDesignPosts = () => {
  const data = useStaticQuery(graphql`
    query {
      design: allFile(
        filter: {
          extension: { regex: "/(mdx)/" }
          sourceInstanceName: { eq: "design" }
        }
      ) {
        nodes {
          childMdx {
            frontmatter {
              title
              slug
              image {
                sharp: childImageSharp {
                  fluid {
                    ...GatsbyImageSharpFluid_withWebp_tracedSVG
                  }
                }
              }
              type
            }
            excerpt
          }
        }
      }
    }
  `)

  return data.design.nodes.map(post => ({
    title: post.childMdx.frontmatter.title,
    slug: post.childMdx.frontmatter.slug,
    image: post.childMdx.frontmatter.image,
    type: post.childMdx.frontmatter.type,
    excerpt: post.excerpt,
  }))
}

export default useDesignPosts
