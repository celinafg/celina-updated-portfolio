import { graphql, useStaticQuery } from "gatsby"

const useProjectPosts = () => {
  const data = useStaticQuery(graphql`
    query {
      projects: allFile(
        filter: {
          extension: { regex: "/(mdx)/" }
          sourceInstanceName: { eq: "projects" }
        }
      ) {
        nodes {
          childMdx {
            frontmatter {
              title
              slug
              tech
              url
              image {
                sharp: childImageSharp {
                  fluid {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
            excerpt
          }
        }
      }
    }
  `)

  return data.projects.nodes.map(post => ({
    title: post.childMdx.frontmatter.title,
    type: post.childMdx.frontmatter.type,
    slug: post.childMdx.frontmatter.slug,
    tech: post.childMdx.frontmatter.tech,
    image: post.childMdx.frontmatter.image,
    url: post.childMdx.frontmatter.url,
    excerpt: post.excerpt,
  }))
}

export default useProjectPosts
