import { graphql, useStaticQuery } from "gatsby"

const useBlogPosts = () => {
  const data = useStaticQuery(graphql`
    query {
      blog: allFile(
        filter: {
          extension: { regex: "/(mdx)/" }
          sourceInstanceName: { eq: "blog" }
        }
      ) {
        nodes {
          childMdx {
            frontmatter {
              title
              slug

              blurb
              type
              date(formatString: "MMMM DD, YYYY")
              image {
                sharp: childImageSharp {
                  fluid {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
            excerpt
          }
        }
      }
    }
  `)

  return data.blog.nodes.map(post => ({
    title: post.childMdx.frontmatter.title,
    slug: post.childMdx.frontmatter.slug,
    image: post.childMdx.frontmatter.image,
    blurb: post.childMdx.frontmatter.blurb,
    type: post.childMdx.frontmatter.type,
    date: post.childMdx.frontmatter.date,
    excerpt: post.excerpt,
  }))
}

export default useBlogPosts
