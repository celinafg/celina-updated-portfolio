import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import { Link } from "gatsby"
import { MDXRenderer } from "gatsby-plugin-mdx"
import { css } from "@emotion/core"
import styled from "@emotion/styled"
import TechPreview from "../components/tech-preview"
import color from "../components/colors"

const PostTemplate = ({ data: { mdx: post } }) => {
  const tags = post.frontmatter.tech

  return (
    <Layout>
      <WrapperDoc>
        <Container>
          <h2
            css={css`
              margin-top: 8vh;
            `}
          >
            {post.frontmatter.title}
          </h2>
          <Flex>
            <Type>{post.frontmatter.type}</Type>

            <Btn>
              <a
                href={post.frontmatter.url}
                target="_blank"
                rel="noopener noreferrer"
              >
                live site
              </a>
            </Btn>
          </Flex>
          <Line />
          <Body>
            <MDXRenderer>{post.body}</MDXRenderer>
          </Body>
          <Title>TOOLS & TECHNOLOGIES</Title>
          <Subhead>tech stack used</Subhead>
          <TechWrap>
            {tags.map(tag => (
              <TechPreview key={tag.slug} tag={tag} />
            ))}
          </TechWrap>

          <Back to="/" className="linky">
            &larr; back to all posts
          </Back>
        </Container>
      </WrapperDoc>
    </Layout>
  )
}

const Btn = styled.button`
  box-shadow: 5px 6px 1px 1px ${color.midnight};

  &:hover {
    cursor: pointer;
    letter-spacing: 0.1em;
  }

  @media (max-width: 768px) {
    margin-top: 20px;
  }
`
const Flex = styled.div`
  display: flex;
  justify-content: space-between;
  margin-right: 5vw;
  align-items: baseline;

  @media (max-width: 768px) {
    flex-wrap: wrap;
    margin-top: 10px;
  }
`
const Subhead = styled.p`
  font-weight: lighter;
  margin: 0;
  padding: 0;
  margin-top: 4px;
`
const Title = styled.h5`
  font-family: Josefin Sans;
  margin-top: 6vh;
  font-weight: bold;
  letter-spacing: 0.15em;
`
const TechWrap = styled.div`
  display: flex;

  flex-wrap: wrap;
  justify-content: space-end;
  margin-top: 1vh;
  margin-bottom: 5vh;
`

const Body = styled.div`
  margin-right: 5.5vw;
  text-align: left;
  text-indent: 10vh;
  width: 100%;

  @media (max-width: 768px) {
    margin-right: 0;
  }
`
const WrapperDoc = styled.div`
  display: flex;
  min-height: 97vh;
  margin: 0 10vh;

  @media (max-width: 768px) {
    margin: 2vh 6vw;
    width: 96%;
  }
`
const Container = styled.div`
  display: flex;
  flex-direction: column;
`

const Type = styled.p`
  text-transform: uppercase;
  letter-spacing: 0.4rem;
  font-size: 0.9rem;
`

const Line = styled.hr`
  width: 7rem;
  height: 0.5vh;
  border: none;
  margin-top: 2.2vh;
  margin-bottom: 5vh;

  @media (max-width: 768px) {
    margin-top: 0;
    margin-bottom: 0;
  }
`

const Back = styled(Link)`
  margin-bottom: 6vh;

  display: block;
`

export default PostTemplate

export const query = graphql`
  query($slug: String!) {
    mdx(frontmatter: { slug: { eq: $slug } }) {
      frontmatter {
        title
        type
        tech
        url
      }
      body
    }
  }
`

// export const query = graphql`
//   query($slug: String!) {
//     allMdx(
//       filter: {
//         internal: { mediaType: { eq: "markdown/text" } }
//         frontmatter: { slug: { eq: $slug } }
//       }
//     ) {
//       edges {
//         node {
//           frontmatter {
//             title
//             type
//             tech
//             slug
//           }
//         }
//       }
//     }
//   }
// `
