import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import { Link } from "gatsby"
import { MDXRenderer } from "gatsby-plugin-mdx"
import { css } from "@emotion/core"
import styled from "@emotion/styled"
import color from "../components/colors"

const BlogTemplate = ({ data: { mdx: post } }) => {
  return (
    <Layout>
      <WrapperDoc>
        <Container>
          <h2
            css={css`
              margin-top: 8vh;
            `}
          >
            {post.frontmatter.title}
          </h2>
          {/* <Flex>
            <Type>{post.frontmatter.type}</Type>

            <Btn>
              <a
                href={post.frontmatter.url}
                target="_blank"
                rel="noopener noreferrer"
              >
                live site
              </a>
            </Btn>
          </Flex> */}
          <Line />
          <Body>
            <MDXRenderer>{post.body}</MDXRenderer>
          </Body>
          <Back to="/blog" className="linky link-margin">
            &larr; back to all posts
          </Back>
        </Container>
      </WrapperDoc>
    </Layout>
  )
}

const Body = styled.div`
  margin-right: 5.5vw;
  width: 100%;

  p {
    text-indent: 10vh;
  }

  h3 {
  }
  img {
    margin-top: 10vh;
  }

  @media (max-width: 768px) {
    margin-right: 0;
    text-indent: 5vw;
  }
`
const WrapperDoc = styled.div`
  display: flex;
  min-height: 96vh;
  margin: 0 10vh;

  @media (max-width: 768px) {
    margin: 2vh 6vw;
    width: 92%;
  }
`
const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

// const Type = styled.p`
//   text-transform: uppercase;
//   letter-spacing: 0.4rem;
//   font-size: 0.9rem;
//   color: ${color.mud};
// `

const Line = styled.hr`
  width: 7rem;
  height: 0.5vh;
  background-color: ${color.mutedBlue};
  border: none;
  margin-top: 2.2vh;
  margin-bottom: 5vh;
`

const Back = styled(Link)`
  margin-bottom: 6vh;
  color: ${color.mustard};
  display: block;

  &:hover {
    border-bottom: 2px solid;
  }
`

export default BlogTemplate

export const query = graphql`
  query($slug: String!) {
    mdx(frontmatter: { slug: { eq: $slug } }) {
      frontmatter {
        title
        type
      }
      body
    }
  }
`
