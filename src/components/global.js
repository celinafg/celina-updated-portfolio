import React from "react"
import { Global, css } from "@emotion/core"
import themes from "./colors"

const GlobalStyles = () => {
  return (
    <>
      <Global
        styles={css`
          * {
            box-sizing: border-box;
            margin: 0;
          }

          html,
          body {
            overflow-x: hidden;
            transition: all 0.25s linear;
            color: ${props => themes[props.theme.name].paper};

            font-family: "Josefin Sans", "sans-serif";
            font-size: 17px;
            line-height: 1.4;
            ${"" /* @media (min-width: calc(550px + 10vw)) {
              font-size: 18px;
            } */}

            > div {
              margin-top: 0;
            }

            .img {
              margin-top: 10vh;
            }

            button {
              border-radius: 15px;
              font-family: "Josefin Sans", "sans-serif";
              font-size: 1rem;
              padding: 0.4rem 2rem;
              text-decoration: none;
              border: 2px solid ${({ theme }) => theme.mint};
              background: none;
              color: ${({ theme }) => theme.mud};
            }
          }
          h1 {
            line-height: 1.1;
            font-size: 3em;
          }
          h2 {
            font-size: 2.5em;
          }
          h3 {
            font-size: 2em;
          }
          h4 {
            font-size: 1.5em;
          }
          h5 {
            font-size: 1em;
          }
          h6 {
            font-size: 0.5em;
          }
          h1,
          h2,
          h3,
          h4,
          h5 {
            font-family: "Yeseva One", "serif";
            letter-spacing: 0.04em;
          }
          h6 {
            font-family: "Josefin Sans", "sans-serif";
            line-height: 1.1;
            + * {
              margin-top: 0.5rem;
            }
          }

          li {
            margin-top: 0.25rem;
          }

          p {
            font-size: 1.1rem;
          }
        `}
      />
    </>
  )
}

export default GlobalStyles
