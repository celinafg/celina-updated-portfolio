import Switch from "react-switch"
import React from "react"
import styled from "@emotion/styled"

const Header = ({ theme }) => (
  <header>
    <Div>
      <Switch
        onChange={() =>
          theme.updateTheme(theme.name === "light" ? "dark" : "light")
        }
        checked={theme.name === "light"}
        onColor="#D8B860"
        offColor="#144B7C"
        boxShadow="0 0 2px 3px #D8B860"
        activeBoxShadow="0 0 2px 3px #9D9A7C"
      />
    </Div>
  </header>
)

const Div = styled.div`
  float: right;
  padding-top: 5vh;
  padding-right: 10vw;
  @media (max-width: 768px) {
    padding-top: 3vh;
  }
`

export default Header
