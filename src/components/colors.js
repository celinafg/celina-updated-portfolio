const themes = {
  dark: {
    mustard: "#D8B860",
    mud: "#8E7979",
    pistachio: "#9D9A7C",
    mint: "#CAE5D3",
    midnight: "#0A3862",
    lightMid: "#4A6C8B",
    mutedBlue: "#144B7C",
    paper: "#BBB0B0",
    postcard: " rgba(20, 75, 124, 0.7)",
    postText: "#0A3862",
  },
  light: {
    mustard: "#1B4973",
    mud: "#226A5B",
    pistachio: "#448D7D",
    mint: "#226A5B",
    midnight: "#EEEFEB",
    lightMid: "#D8B860",
    mutedBlue: "#E4E3D8",
    paper: "#B07575",
    postcard: " rgba(232, 213, 161, 0.8)",
    postText: "#226A5B",
  },
}

export default themes
