import { Link } from "gatsby"
import styled from "@emotion/styled"
import Image from "gatsby-image"
import { css } from "@emotion/core"
import React from "react"
const ProjectPreview = ({ post }) => {
  const slug = post.slug
  return (
    <Article>
      <Link
        to={`/projects/${slug}`}
        css={css`
          margin: 2rem 0 0 0;
        `}
      >
        <Card>
          <Img
            fluid={post.image.sharp.fluid}
            alt={post.title}
            objectFit="cover"
          />

          <Info>
            <h3>{post.slug}</h3>
            <Link to={post.slug}>
              <Span>&rarr;</Span>
            </Link>
          </Info>
        </Card>
      </Link>
    </Article>
  )
}

const Card = styled.div`
  @media (max-width: 768px) {
    height: 36vh;
    margin-bottom: 7vh;
  }
`

const Img = styled(Image)`
  margin: 0;
  height: 38vh;

  @media (max-width: 768px) {
    height: 100%;
  }
`

const Article = styled.article`
  height: 48vh;
  width: 38vw;
  margin-top: 6vh;
  margin-right: 10px;
  &:hover {
    box-shadow: 0 2.8px 2.2px rgba(0, 0, 0, 0.034),
      0 6.7px 5.3px rgba(0, 0, 0, 0.048), 0 12.5px 10px rgba(0, 0, 0, 0.06),
      0 22.3px 17.9px rgba(0, 0, 0, 0.072), 0 41.8px 33.4px rgba(0, 0, 0, 0.086),
      0 100px 80px rgba(0, 0, 0, 0.12);
  }
  @media (max-width: 768px) {
    width: 80vw;
    height: 30%;
    margin-top: 4vh;
    margin-right: 0;
  }
`
const Span = styled.span`
  margin-left: 30px;
  text-align: right;
`
const Info = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  align-content: baseline;
  margin-top: 10px;
  padding-bottom: 10px;

  @media (max-width: 768px) {
    height: 6vh;
  }
`

export default ProjectPreview
