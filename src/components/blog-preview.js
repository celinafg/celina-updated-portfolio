import React from "react"
import { Link } from "gatsby"
import styled from "@emotion/styled"
import Image from "gatsby-image"
import { css } from "@emotion/core"

const BlogPreview = ({ post }) => {
  const slug = post.slug
  return (
    <>
      <article
        css={css`
          display: flex;
          flex-direction: column;

          :first-of-type {
            margin-top: 1rem;
          }
        `}
        className="border"
      >
        <Container>
          <Text>
            <Type>{post.type}</Type>

            <H3 className="linky">
              <Linki to={`/blog/${slug}`}>
                {post.title}
                <Arrow>&rarr;</Arrow>
              </Linki>
            </H3>

            <Dates>{post.date}</Dates>
            <Blurb>{post.blurb}</Blurb>
          </Text>
          <Img
            fluid={post.image.sharp.fluid}
            alt={post.title}
            objectFit="cover"
          />
        </Container>
      </article>
    </>
  )
}

const Linki = styled(Link)`
  display: flex;
  align-items: center;
`
const Arrow = styled.span`
  font-size: 20px;
  margin-left: 10px;
`

const H3 = styled.h3`
  text-align: left;
  @media (max-width: 768px) {
    width: 80vw;
  }
`
const Dates = styled.div`
  width: 100%;
  font-size: 15px;
  font-style: italic;

  @media (max-width: 768px) {
   margin-top: 6px;
  }
}
`
const Text = styled.div`
  width: 100%;
  min-height: 20vh;
  display: flex;
  flex-direction: column;
`

const Blurb = styled.p`
  width: 85%;
  height: 60%;
  margin-top: 10px;
  @media (max-width: 768px) {
    width: 100%;
  }
`

const Container = styled.div`
  display: flex;
  padding: 17px 0;

  @media (max-width: 768px) {
    flex-wrap: wrap;
    padding: 12px 0;
    height: 100%;
  }
`

const Img = styled(Image)`
  margin: 0;
  min-height: 25vh;

  width: 30vw;
  @media (max-width: 768px) {
    display: none;
  }
`

const Type = styled.p`
  text-transform: uppercase;
  letter-spacing: 0.2rem;
  font-size: 0.6rem;
  margin-top: 10px;
  margin-bottom: 5px;
`

export default BlogPreview
