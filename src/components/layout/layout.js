import React from "react"
import styled from "@emotion/styled"
import { css } from "@emotion/core"
import useSiteMetadata from "../../hooks/use-sitemetadata"
import { Link } from "gatsby"
import "./layout.css"
import "./cc.png"
import { TiThMenu } from "react-icons/ti"
import Helmet from "react-helmet"
import themes from "../colors"
import { ThemeContext } from "../ThemeContext"
import GlobalStyles from "../global"
import Header from "../../components/header"

const logocon = require("./cc.png")

const ThemedLayout = styled.div`
  color: ${props => themes[props.theme.name].mud};
  background-color: ${props => themes[props.theme.name].midnight};
  transition: all 0.4s ease;

  button {
    border: 2px solid ${props => themes[props.theme.name].pistachio};
    background: none;
    color: ${props => themes[props.theme.name].pistachio};
  }
  h1,
  h2,
  h3,
  h4 {
    color: ${props => themes[props.theme.name].mustard};
  }
  h5 {
    color: ${props => themes[props.theme.name].pistachio};
  }
  h6 {
    color: ${props => themes[props.theme.name].pistachio};
  }
  & hr {
    width: 7rem;
    height: 0.5vh;
    background-color: ${props => themes[props.theme.name].lightMid};
    border: none;
    margin-top: 2vh;
    margin-bottom: 4vh;
  }
  .link-margin {
    margin-bottom: 4vh;
    margin-top: 5vh;
  }
  .linky {
    color: ${props => themes[props.theme.name].pistachio};

    &:hover {
      color: ${props => themes[props.theme.name].mustard};
    }
  }
  .techwrap {
    background: ${props => themes[props.theme.name].mutedBlue};
  }
  .border {
    border-bottom: 1px solid ${props => themes[props.theme.name].lightMid};
  }
  .img__description_layer {
    background-color: ${props => themes[props.theme.name].postcard};
  }
  .extra {
    margin-top: 37px;
  }
  .footer {
    color: ${props => themes[props.theme.name].mud};
  }
  .overlay {
    &:hover {
      background-color: rgba(152, 179, 203, 0.5);
    }
  }
  .post {
    border: 6px solid ${props => themes[props.theme.name].postcard};
  }
  .card {
    background: ${props => themes[props.theme.name].postcard};

    height: 10%;
  }
`

const Layout = ({ children, props }) => {
  const { title, description } = useSiteMetadata()

  return (
    <ThemeContext.Consumer>
      {theme => (
        <ThemedLayout theme={theme}>
          <>
            <GlobalStyles />
            <Helmet>
              <html lang="en" />
              <title>Celina Garcia</title>
              <meta name="description" content={description} />
            </Helmet>
            <div className="s-layout">
              <header>
                <div className="s-layout__sidebar">
                  <a className="s-sidebar__trigger" href="#0">
                    <Header theme={theme} />
                    <p className="icon">
                      <TiThMenu />
                    </p>
                  </a>

                  <nav className="s-sidebar__nav">
                    <Wrapper>
                      <ul>
                        <li>
                          <NavLink>
                            <Img src={logocon} className="image" />
                          </NavLink>
                        </li>
                        <div className="text">
                          {/* <li>
                            <NavLink
                              activeClassName="current-page"
                              className="s-sidebar__nav-link"
                              href="#0"
                            >
                              <em>About</em>
                            </NavLink>
                          </li> */}
                          <li>
                            <NavLink
                              activeClassName="current-page"
                              className="s-sidebar__nav-link"
                              href="#projects"
                            >
                              <em>Projects</em>
                            </NavLink>
                          </li>
                          <li>
                            <NavLink
                              activeClassName="current-page"
                              className="s-sidebar__nav-link"
                              href="#0"
                              to="/design"
                            >
                              <em>Art + Design</em>
                            </NavLink>
                          </li>
                          <li>
                            <NavLink
                              activeClassName="current-page"
                              className="s-sidebar__nav-link"
                              href="#0"
                              to="/blog"
                            >
                              <em>Blog</em>
                            </NavLink>
                          </li>
                          {/* <li>
                            <NavLink className="s-sidebar__nav-link" href="#0">
                              <em>Contact</em>
                            </NavLink>
                          </li> */}
                          <li>
                            <NavLink
                              className="s-sidebar__nav-link"
                              href="#0"
                              to="/resume"
                            >
                              <em>Resume</em>
                            </NavLink>
                          </li>
                        </div>
                      </ul>
                    </Wrapper>
                  </nav>
                </div>
              </header>

              <main className="s-layout__content">
                <div>
                  <div
                    css={css`
                      margin: 0 10px;
                    `}
                  >
                    <Header theme={theme} className="hide" />
                    {children}
                  </div>
                </div>
                <footer className="footer">
                  <small>
                    This site was made with Gatsby and GraphQL! @2020
                  </small>
                </footer>
              </main>
            </div>
          </>
        </ThemedLayout>
      )}
    </ThemeContext.Consumer>
  )
}
const Img = styled.img`
  height: 9rem;
  display: flex;
  padding-left: 3rem;
`

const NavLink = styled(Link)`
  text-transform: uppercase;
  font-size: 1rem;

  &:last-of-type {
    margin-right: 0;
  }
`

const Wrapper = styled.div`
  margin-top: 3rem;
`

export default Layout
