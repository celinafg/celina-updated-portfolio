import React from "react"
// import { css } from "@emotion/core"
// import { Link } from "gatsby"
import styled from "@emotion/styled"

const TechPreview = ({ tag }) => (
  <Article className="techwrap">
    <P>{tag}</P>
  </Article>
)

const P = styled.h6`
  margin: 0 auto;
  padding: 0;
  display: inline-block;
  line-height: 50px;
  text-align: center;
  font-size: 18px;
  font-weight: normal;
`
const Article = styled.article`
  text-align: center;

  width: 12vw;
  height: 7vh;
  margin-right: 10px;
  margin-bottom: 1vh;
  ${"" /* background: #1b4973; */}
  border-radius: 10px;
  @media (max-width: 768px) {
    width: 30%;
  }
`

export default TechPreview
