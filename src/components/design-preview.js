import React from "react"
// import { css } from "@emotion/core"
import { Link } from "gatsby"
import styled from "@emotion/styled"
import Image from "gatsby-image"

const DesignPreview = ({ post }) => {
  const slug = post.slug
  return (
    <Article>
      <Link
        to={`/design/${slug}`}
        // css={css`
        //   margin: 2rem 0 0 0;
        // `}
      >
        <div className="img__wrap">
          <Img
            fluid={post.image.sharp.fluid}
            objectPosition="50% 50%"
            alt={post.title}
          />
          <div className="img__description_layer">
            <P className="img__description">{post.title}</P>
          </div>
        </div>
      </Link>
    </Article>
  )
}

const P = styled.h3`
  transition: 0.2s;
  transform: translateY(1em);
  width: 18vw;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Img = styled(Image)`
  width:auto;
  height: 52vh;
  margin: 0;
  }

  @media (max-width: 768px) {
    min-width: 85vw;
    height: 100%;
  }
`

const Article = styled.article`
  margin: 20px 10px;
  @media (max-width: 768px) {
    height: 100%;
    margin-right: 0;
  }
`

export default DesignPreview
